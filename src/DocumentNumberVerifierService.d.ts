import {ApplicationService, IApplication} from '@themost/common';
import { DataContext } from '@themost/data';

declare interface VerifyCodeParams {
    documentSeries?: any;
    documentNumber?: any
}

declare class DocumentNumberVerifierService extends ApplicationService  {
    constructor(app: IApplication);

    generateURI(documentSeries: any, documentNumber: any): string;
    getVerifierURI(): string;
    generateShortDocumentCode(context: DataContext | any): string;

}

export {
    VerifyCodeParams,
    DocumentNumberVerifierService
}
